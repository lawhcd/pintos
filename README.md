Pintos is a simple operating system framework for the 80x86 architecture. It supports kernel threads, loading and running user programs, and a file system.
These core functionalities have been strenghtened and a virtual memory implementation added.

Tasks completed:
- Thread scheduling and priority donation
- User programs with system calls, argument passing and termination messages
- Virtual memory with paging, stack growth and memory mapped files